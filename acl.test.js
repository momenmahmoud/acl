const acl = require('./acl');
const roles = require('./roles');

// role creation
test('creating a role adds it in the roles list', () => {
    const roleName = 'admin';
    acl.createRole(roleName);
    expect(roles).toMatchObject({ [roleName]: {} })
});

// testing the following line :
// an('admin').can('post').to('/:userId/posts').when((params,user)=>params.userId === user.id)
test('the a/an function returns an object with a can function', () => {
    const whenMockFunctionArg = jest.fn((params,user)=>params.userId === user.id)

    const anSpy = jest.spyOn(acl, 'an');
    const anReturnValue = acl.an('admin');

    const canSpy = jest.spyOn(anReturnValue,'can');
    const canReturnValue = anReturnValue.can('post');

    const toSpy = jest.spyOn(canReturnValue,'to');
    const toReturnValue = canReturnValue.to('/:userId/posts');

    const whenSpy = jest.spyOn(toReturnValue,'when');
    const whenReturnValue = toReturnValue.when(whenMockFunctionArg);

    expect(anSpy).toHaveBeenCalled();
    expect(anReturnValue).toHaveProperty('can');

    expect(canSpy).toHaveBeenCalledWith('post');
    expect(canReturnValue).toHaveProperty('from');

    expect(toSpy).toHaveBeenCalledWith('/:userId/posts');
    expect(toReturnValue).toHaveProperty('when');

    expect(whenSpy).toHaveBeenCalledWith(whenMockFunctionArg);

    anSpy.mockRestore();
    canSpy.mockRestore();
    toSpy.mockRestore();
    whenSpy.mockRestore();
})

// checking the following line:
// acl.check.if('admin').can('post').to('/:userId/posts')
// acl.check.if('admin').can('post').to('/10/posts').when({id:10})
test('calling check function', () => {
    const checkReturnValue = acl.check;
    expect(checkReturnValue).toHaveProperty('if');

    const ifSpy = jest.spyOn(checkReturnValue,'if');
    const ifReturnValue = checkReturnValue.if('admin');

    const canSpy = jest.spyOn(ifReturnValue,'can');
    const canReturnValue = ifReturnValue.can('post');

    const toSpy = jest.spyOn(canReturnValue,'to');
    const toReturnValue = canReturnValue.to('/:userId/posts');

    const toReturnValueWithUserId = canReturnValue.to('/10/posts');

    const userData = {id:10};
    const whenSpy = jest.spyOn(toReturnValueWithUserId,'when');
    const whenReturnValue = toReturnValueWithUserId.when(userData);

    expect(ifSpy).toHaveBeenCalledWith('admin');
    expect(ifReturnValue).toHaveProperty('can');

    expect(canSpy).toHaveBeenCalledWith('post');
    expect(canReturnValue).toHaveProperty('to');

    expect(toSpy).toHaveBeenCalledWith('/:userId/posts');
    expect(toReturnValue).toBeTruthy();

    expect(toReturnValueWithUserId).toHaveProperty('when');

    expect(whenSpy).toHaveBeenCalledWith(userData);
    expect(whenReturnValue).toBeTruthy();
});

