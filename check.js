const checkWhen = (targetPath, path) => (data) => {
  if (!targetPath.when) return 'no when function to check';
  if (!data || !Object.keys(data).length) return 'no data to check'; 
  const params = targetPath.match(path).params;
  Object.keys(data).forEach(key => {data[key] = data[key].toString()})
  return targetPath.when(params, data);
} 

const checkPath = (targetMethod) => (path) => {
  if (targetMethod[path]) return true;
  const existPath = Object.keys(targetMethod).filter(key => targetMethod[key].regex.exec(path))[0];
  if (!targetMethod[existPath]) return false;
  return { when: checkWhen(targetMethod[existPath], path) };
}

const checkMethod = (targetRole) => (method) => ({
  from: checkPath(targetRole[method]),
  to: checkPath(targetRole[method]),
});


const check = (roles) => ({
  if: (roleName) => ({
    can: checkMethod(roles[roleName]),
  }),
});


module.exports = check;
