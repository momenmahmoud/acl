const roles = require('./roles');
const an = require('./create');
const check = require('./check');

const createRole = (roleName) => { roles[roleName] = {}; };

const acl = {
  createRole,
  an: an(roles),
  a: an(roles),
  check: check(roles),
};

module.exports = acl;
